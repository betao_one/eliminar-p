import { Component } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  title = 'uploadImg';

  constructor(private httpClient: HttpClient) { }

  selectedFile: File;
  selectedFileXML: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  public onFileChangedPDF(event) {
    //Select File
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }
  public onFileChangedXML(event) {
    //Select File
    this.selectedFileXML = event.target.files[0];
    console.log(this.selectedFileXML);
  }

  //Gets called when the user clicks on submit to upload the image

  onUpload() {
    let json = {
      "idComprobanteViatico": "1",
      "fecha": "2020-12-10",
      "subCuentaContable": "1",
      "aprobacionContador": true,
      "aprobacionGerente": true,
      "aprobacionPrestador": true,
      "estatusComprobante": "5",
      "observaciones": "OK",
      "rutaXml": "",
      "rutaPdf": "",
      "numeroSolicitud": "11"
    };

    //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.

    const dataComprobante = new FormData();
    dataComprobante.append('pdf', new Blob([this.selectedFile]), this.selectedFile.name);
    dataComprobante.append('comprobante', JSON.stringify(json));
    dataComprobante.append('xml', new Blob([this.selectedFileXML]), this.selectedFileXML.name);


    console.log(dataComprobante.get('comprobante'));

    //Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://dbf3136ebad0.ngrok.io/viaticos-usuario/solicitudes-de-viaticos/34/comprobantes',
      dataComprobante, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.message = 'Image uploaded successfully';
        } else {
          this.message = 'Image not uploaded successfully';
        }
      }
      );

  }


  //Gets called when the user clicks on retieve image button to get the image from back end

  getImage() {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://d3477696406d.ngrok.io/noltik_api/v1/lab/lab05')
      .subscribe(
        res => {
          console.log(res)


          this.retrieveResonse = res;
          console.log(this.retrieveResonse);
          this.base64Data = this.retrieveResonse.body.imgByte;

          console.log(this.base64Data);

          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;

        }
      );
  }

}
